<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Repositories\Eloquent\UserEloquentRepository;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
class UserController extends Controller
{
    protected $userRepository;

    function __construct(
        UserEloquentRepository $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.user.update');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        return $request->all();
        if ($request->hasFile('avatar')) {
            $image = $request->file('avatar');
            //rename
            $filename = str_random(4) . $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(320, 200);
            $image_resize->save(public_path('uploads/' . $filename));
            $urlFile = 'uploads/' . $filename;
        }else{
            $urlFile = Auth::user()->avatar;
        }
        ////upload avartar if use
        $data = [
            'phone' => $request->get('phone'),
            'email' => $request->get('email'),
            'avatar' => $urlFile,
            'name' => $request->get('name'),
            'birth_day' => $request->get('birth_day'),
            'nickname' => $request->get('nickname'),
        ];
        $user = $this->userRepository->update($data,$id);
        if ($user) {
            return redirect()->back()->with('message', 'Update successful!');
        }
        //update profile
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
