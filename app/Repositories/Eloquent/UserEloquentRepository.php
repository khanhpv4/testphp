<?php
namespace App\Repositories\Eloquent;


use App\Repositories\BaseEloquentRepository;
use App\User;

class UserEloquentRepository extends BaseEloquentRepository
{

     /**
     * @return mixed
     */
    public function model()
    {
        return User::class;
    }

}
