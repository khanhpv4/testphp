@extends('admin.dashboard')
@section('content')
    <h1>Profile</h1>
    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </div>
    </div>
    <form action="{{route('user-profile.update',['id'=>Auth::user()->id])}}" method="post" enctype="multipart/form-data">
        @csrf
        {{ method_field('PUT') }}
        <div class="form-group">
            <label for="exampleInputEmail1"><strong class="text-danger">*</strong>  Email address</label>
            <input type="email" class="form-control" id="email" min="10" max="50" value="{{Auth::user()->email}}"  name="email" required>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Phone</label>
            <input type="text" class="form-control" id="phone" minlength="9" maxlength="20" value="{{Auth::user()->phone}}" placeholder="Enter your phone  max 20" name="phone">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1"><strong class="text-danger">*</strong>   Name</label>
            <input type="text" class="form-control"  id="name" minlength="3" maxlength="30" value="{{Auth::user()->name}}" placeholder="Enter your full name max 30 " name="name" required>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Nick Name</label>
            <input type="text" class="form-control"  id="nickname" maxlength="50" value="{{Auth::user()->nickname}}" placeholder="Enter your nickname" name="nickname">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Birth day</label>
            <input type="date" value="{{Auth::user()->birth_day}}" class="form-control"  id="birth_day" maxlength="50" value="{{Auth::user()->nickname}}" placeholder="Enter your birth day max 50" name="birth_day">
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputEmail1">Avatar</label>
                    <input type="file" class="form-control" name="avatar" id="avatar" accept="image/x-png,image/gif,image/jpeg">
                </div>
            </div>
            <div class="col-md-6">
                <img width="40%" src="{{asset(Auth::user()->avatar)}}" alt="">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>


@endsection